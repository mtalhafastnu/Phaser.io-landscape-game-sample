var scores = function(game) {};

scores.prototype = {
    init: function () {
        this.titleText = game.make.text(game.world.centerX, 100, "Game Title", {
            font: 'bold 50pt TheMinion',
            fill: '#ff8000',
            align: 'center',
            stroke: "green",
            strokeThickness : 10,
        });
        this.titleText.setShadow(3, 3, 'rgba(0,0,0,0.5)', 5);
        this.titleText.anchor.set(0.5);
        game.add.tween(this.titleText.scale).to({ x: 0.8, y: 0.8}, 700, Phaser.Easing.Linear.None, true, 500, 20, true);  

        this.stateText = game.make.text(game.world.centerX, game.world.centerY/2, "High Scores", {
            font: 'bold 40pt TheMinion',
            fill: '#ff9f00',
            align: 'center',
            stroke: "#000000",
            strokeThickness : 5,
        });
        this.stateText.setShadow(3, 3, 'rgba(0,0,0,0.5)', 5);
        this.stateText.anchor.set(0.5);
        
        this.homeIcon = this.add.sprite(0, 0, 'icons', 99);
        this.homeIcon.inputEnabled = true;
        this.homeIcon.events.onInputUp.add(this.clickMusic, this);
        this.homeIcon.events.onInputUp.add(function () {
            game.state.start("gameMenu");   
        });
        this.optionCount = 1;
        
        if (Object.prototype.toString.call( PLAYER_DATA ) !== '[object Array]' ) {
            this.scoreText = game.make.text(game.world.centerX, game.world.centerY/2 + 100, "00", {
                font: 'bold 40pt TheMinion',
                fill: '#ffffff',
                align: 'center',
                stroke: "#000000",
                strokeThickness : 10,
            });
            this.scoreText.setShadow(3, 3, 'rgba(0,0,0,0.5)', 5);
            this.scoreText.anchor.set(0.5);
        }else{
            this.scoreText = game.make.text(game.world.centerX, game.world.centerY/2 + 100, PLAYER_DATA[1], {
                font: 'bold 50pt TheMinion',
                fill: '#ffffff',
                align: 'center',
                stroke: "#000000",
                strokeThickness : 10,
            });
            this.scoreText.setShadow(3, 3, 'rgba(0,0,0,0.5)', 5);
            this.scoreText.anchor.set(0.5);
        }
    },
    create: function () {
        game.add.existing(this.titleText);
        game.add.existing(this.stateText);
        game.add.existing(this.scoreText);
    },
    clickMusic: function(){
        clickmusic.stop();
        if(gameOptions.playSound == true){
            if (game.cache.isSoundDecoded('clickmusic')){
                clickmusic.play();
            }

        }
    },
};